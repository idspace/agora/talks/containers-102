```
docker volume create nginx_data
docker run --rm -d -v nginx_data:/usr/share/nginx/html/ nginx:latest
docker run --rm -it -v $(pwd):/backup --volumes-from nginx nginx:latest tar cvf /backup/backup.tar /usr/share/nginx/html/
```
