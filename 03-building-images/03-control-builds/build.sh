reverse_proxy="nginx"

container=$(buildah from oraclelinux:9)

if [[ $reverse_proxy = nginx ]]; then
    buildah run $container -- yum install -y nginx;
else
    buildah run $container -- yum install -y httpd;
fi

buildah config --port 80 $container
buildah commit $container reverse_proxy
buildah tag reverse_proxy docker.io/iduoad/reverse_proxy:0.1

