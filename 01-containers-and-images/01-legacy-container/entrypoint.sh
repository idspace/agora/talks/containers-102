#!/bin/bash

echo -e "Hello! I am a legacy application! I don't write logs to stdout!\n"

echo "My logs goes here" > /var/log/legacy-logs.txt

echo "I write other things everywhere" > /var/cache/legacy-logs.txt

echo -e "Doing Work! Failing soon"

exit 1
