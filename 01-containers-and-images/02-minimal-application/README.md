Run the container
```
docker run -it --rm --name test --dns 12.12.12.12 test:latest
```

ctr=(docker inspect test --filter '{{ .State.Pid }}')

sudo nsenter -t $ctr -n /bin/nslookup www.oracle.com 12.12.12.12
