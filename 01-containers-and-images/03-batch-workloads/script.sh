# Job 1
echo "INFO: Running job1"
job1=$(podman run -d --rm oraclelinux:9-slim sleep 5)
exit_code=$(podman wait $job1)
test $exit_code -eq 0 || echo "Job1 failed with exit status $exit_code \n"

# Job 2
echo "INFO: Running job2"
job2=$(podman run -d --rm oraclelinux:9-slim /bin/sh -c 'sleep 5 && exit 1')
exit_code=$(podman wait $job2)
test $exit_code -eq 0 || echo "ERROR: Job1 failed with exit status $exit_code \n"
