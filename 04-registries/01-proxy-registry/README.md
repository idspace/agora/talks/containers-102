```
docker run -d -p 5000:5000 --restart always --name registry registry:2
docker tag test:latest localhost:5000/test:latest
docker push --tls-verify=false localhost:5000/test:latest
skopeo sync --src docker --dest docker --dest-tls-verify=false --scoped docker.io/iduoad/students localhost:5000/iduoad/students
```
